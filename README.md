# RangL soft launch

Welcome to the RangL soft launch repository!

To install the RangL environment on your local machine, 

1. If necessary, install the pip package manager (you can do this by running the `get-pip.py` Python script)

2. Run `pip install -e environment`

## Developing your agent

To get started with developing your agent for this RangL challenge, head into the `environment` folder and check out the README there.

### Note: Early access

This repository provides early access to a Beta version of the RangL environment code before the challenge opens on 18th January. Early feedback can be shared in the #askaway channel on Slack (or by creating a GitLab issue). Code for 
* evaluating your agents 
* submitting them to the RangL competition platform  

will be added here shortly.
