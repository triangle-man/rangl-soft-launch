# Quick start guide

After reading the [challenge overview](http://51.140.30.21:8888/web/challenges/challenge-page/21/overview), try an agent by running the `test_reference_environment.py` Python script. This will
* Run a rule-based agent in the environment, and create a plot `fixed_policy.png` of the episode
* Train an RL agent (using just one episode, so it probably won't win!) and create a plot `agent_test.png` of some of the episode
* Save the RL agent as `MODEL_0.zip`


## Folder contents

This folder also contains:

* The `reference_environment` folder. To modify it for development purposes, check out the folder's README.
* `trainer.py`, a Python script containing the `Trainer` class. This class has the `train_rl` method, which is an easy way to train an RL agent on the RangL reference environment and save it.
