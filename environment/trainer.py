import time

from stable_baselines3 import PPO
from stable_baselines3.ppo import MlpPolicy


class Trainer:
    def __init__(self, env):
        self.env = env
        self.param = env.param

    def train_rl(self, models_to_train=40, episodes_per_model=100):
        # specify the RL algorithm to train (eg ACKTR, TRPO...)
        model = PPO(MlpPolicy, self.env, verbose=1)
        start = time.time()

        for i in range(models_to_train):
            steps_per_model = episodes_per_model * self.param.steps_per_episode
            model.learn(total_timesteps=steps_per_model)
            model.save("MODEL_" + str(i))

        end = time.time()
        print("time (min): ", (end - start) / 60)
